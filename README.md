# Coffee Shop App

This project is a quick proof of concept to select Food and Beverages to order and checkout with a simulated payment gateway.

## How to run the project

```
unzip the folder
cd to the folder
run npm install
run npm run start

The application will run in localhost on port 4200
```
