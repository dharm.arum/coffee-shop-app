import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgWizardConfig, NgWizardModule, THEME, TOOLBAR_POSITION} from "ng-wizard";
import {CoffeeAppService} from "./service/coffeeapp.service";
import {HttpClientModule} from "@angular/common/http";

const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default,
  toolbarSettings: {
    toolbarPosition: TOOLBAR_POSITION.none
  }
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgWizardModule.forRoot(ngWizardConfig)
  ],
  providers: [CoffeeAppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
