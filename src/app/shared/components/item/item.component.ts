import {Component, Input, OnInit} from '@angular/core';
import {IItem} from "../../../interfaces/ICoffeeMenu";
import {CoffeeAppService} from "../../../service/coffeeapp.service";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent implements OnInit {

  @Input()
  public items: IItem[];
  //
  @Input()
  public isCartView: boolean
  //
  constructor(public coffeeAppService: CoffeeAppService) { }

  ngOnInit(): void {
  }

  onValueChange(pNumber: number, item?: IItem) {
    if (item) {
      item.quantity = pNumber;
      if (pNumber > 0) {
        this.coffeeAppService.addToCart(item)
      } else {
        this.coffeeAppService.removeFromCart(item)
      }
    }
  }

}
