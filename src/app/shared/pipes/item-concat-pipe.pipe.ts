import { Pipe, PipeTransform } from '@angular/core';
import {IItem} from "../../interfaces/ICoffeeMenu";

@Pipe({
  name: 'itemConcatPipe'
})
export class ItemConcatPipePipe implements PipeTransform {

  transform(items: IItem[], propertyToConcat: string): string {
    let concatenatedProperty: string = ''
    concatenatedProperty += items.map(item => item[propertyToConcat])
    return concatenatedProperty;
  }

}
