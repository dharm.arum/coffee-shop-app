import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { ItemComponent } from './components/item/item.component';
import {NumberPickerModule} from "ng-number-picker";
import { ItemConcatPipePipe } from './pipes/item-concat-pipe.pipe';


@NgModule({
  declarations: [
    ItemComponent,
    ItemConcatPipePipe
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    NumberPickerModule
  ],
  exports: [ItemComponent, ItemConcatPipePipe]
})
export class SharedModule { }
