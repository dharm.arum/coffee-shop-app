import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ICart, ICartItemWiseDiscount, ICoffeeMenu, IItem} from "../interfaces/ICoffeeMenu";
import {Observable} from "rxjs";
import {distinctUntilChanged, map} from "rxjs/operators";
import {IDiscount} from "../interfaces/IDiscount";

/**
 * CoffeeApp service is a service layer responsible to fetch the data from .json or (DB)
 * does all the business logic like add to cart, remove from cart, billing calculations.
 * */
@Injectable({
  providedIn: 'root'
})
export class CoffeeAppService {

  private cart: ICart;
  private discount: IDiscount[];
  //
  constructor(private httpClient: HttpClient) {
    this.cart = {
      items: []
    }
    this.getDiscountData().subscribe((res) => {
      this.discount = res;
    });
  }

  getDiscountData(): Observable<IDiscount[]> {
    return this.httpClient.get<IDiscount[]>('/assets/discount.json').pipe(
      map(res =>  res['discount'] as IDiscount[])
    )
  }

  getCoffeeMenu(): Observable<ICoffeeMenu> {
    return this.httpClient.get('/assets/coffeeapp.json').pipe(
     map(res =>  res as ICoffeeMenu)
    )
  }

  /**
   * add an item to cart
   * */
  addToCart(pItem: IItem) {
    let cartItems: IItem[] = this.cart.items.filter( item => item.id === pItem.id);
    if (cartItems.length === 1) {
      cartItems[0].quantity = pItem.quantity
    } else if (cartItems.length > 1) {
      throw new Error("Duplicate cart items. Internal error occurred");
    } else {
      this.cart.items.push(pItem);
    }
  }

  /**
   * removes an item from cart
   * */
  removeFromCart(pItem: IItem) {
    let itemIndex: number = this.cart.items.findIndex( item => item.id === pItem.id);
    if (itemIndex > -1) {
      this.cart.items.splice(itemIndex , 1);
    }
    console.log(this.cart);
  }

  getCart():ICart {
    return this.cart;
  }

  /**
   * returns item total
   * */
  getItemTotal(): number {
    let itemTotal: number = this.cart.items.reduce<number>((acc: number, current: IItem) => {
      return acc + (current.price * current.quantity)
    }, 0);
    return itemTotal;
  }
  /**
   * Calculates item wise discounts and returns array of ICartItemWiseDiscount
   * */
  getItemWiseDiscount(): ICartItemWiseDiscount[] {
    let cartItemWiseDiscount: ICartItemWiseDiscount[] = [];
    let discountForFilteredItems: number;
    let alreadyDiscountedItemIds: string[] = [];
    //
    const isDiscountAlreadyGiven = (itemIds: string[], discountedIds: string[]): boolean => {
      const alreadyDiscounted = discountedIds.some((discountedItemId : string) => itemIds.some( itemId => itemId === discountedItemId));
      return alreadyDiscounted;
    }
    if (this.discount) {
      this.discount.forEach((discount: IDiscount)=>{
        const discountedItem = discount.itemIds.every( (discountItemId: string) => this.getCart().items.some(itemId => itemId.id === discountItemId));
        if (discountedItem && !isDiscountAlreadyGiven(discount.itemIds, alreadyDiscountedItemIds)) {
          const discountedItemIds: string [] = discount.itemIds;
          let itemsFilteredForDiscountCalc: IItem[] = [];
          alreadyDiscountedItemIds = [...alreadyDiscountedItemIds, ...discount.itemIds];
          discountForFilteredItems = discount.discountPercentage;
          discountedItemIds.forEach( (itemId: string) => {
            itemsFilteredForDiscountCalc.push(this.getCart().items.filter( (item: IItem) => {
              return item.id === itemId;
            })[0])
          })
          //
          const min: number = Math.min(...itemsFilteredForDiscountCalc.map(item => item.quantity));
          const max: number = Math.max(...itemsFilteredForDiscountCalc.map(item => item.quantity));
          const totalPrice = itemsFilteredForDiscountCalc.reduce((acc: number , prev: IItem) => {
            return acc + prev.price
          }, 0)
          const itemIdsToAddInCartWiseDiscount: IItem [] = [];
          itemsFilteredForDiscountCalc.forEach((item:IItem) => {
            itemIdsToAddInCartWiseDiscount.push(item);
          })
          cartItemWiseDiscount.push({
            itemIds: itemIdsToAddInCartWiseDiscount,
            discount: discountForFilteredItems,
            comboQuantity: min,
            comboPrice: totalPrice
          })
          console.log(cartItemWiseDiscount);
          //
        }
      })
    }
    return cartItemWiseDiscount;
  }
  /**
   * returns the discount in percentage. Negates discount calculation in case of duplicate items in combos
   * @Deprecated
   * */
  /*
  getDiscountInPercentage(): number {
    let totalDiscount: number = 0;
    let discountedItemIds: string[] = [];
    const isDiscountAlreadyGiven = (itemIds: string[], discountedIds: string[]): boolean => {
      const alreadyDiscounted = discountedIds.some((discountedItemId : string) => itemIds.some( itemId => itemId === discountedItemId));
      return alreadyDiscounted;
    }
    if (this.discount) {
      this.discount.forEach((discount: IDiscount) => {
        const applicableForDiscount: boolean = discount.itemIds.every((itemId: string) => this.getCart().items.some((item: IItem) => item.id === itemId));
        if(applicableForDiscount && !isDiscountAlreadyGiven(discount.itemIds, discountedItemIds)) {
          discountedItemIds = [...discountedItemIds, ...discount.itemIds];
          totalDiscount += discount.discountPercentage;
        } else {
        }
      })
    }
    return totalDiscount;
  }*/

  /**
   * returns total price after item wise discount
   * */
  getTotal(): number {
    const totalDiscountToApply: number = this.getItemWiseDiscount().reduce(
      (acc: number, current: ICartItemWiseDiscount) => acc + (current.discount / 100) * (current.comboQuantity * current.comboPrice)
      , 0);
    return this.getItemTotal() - totalDiscountToApply;
  }

  resetCart() {
    this.cart = {
      items: []
    }
  }
}
