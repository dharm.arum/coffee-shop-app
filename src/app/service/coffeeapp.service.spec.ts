import { TestBed } from '@angular/core/testing';

import { CoffeeappService } from './coffeeapp.service';

describe('CoffeeappService', () => {
  let service: CoffeeappService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoffeeappService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
