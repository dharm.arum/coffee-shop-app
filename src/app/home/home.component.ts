import { Component, OnInit } from '@angular/core';
import {NgWizardService} from "ng-wizard";

/**
 * Main component that displays wizard
 * */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  public proceedToPayment: boolean = false;

  constructor(private ngWizardService: NgWizardService) { }

  ngOnInit(): void {
  }

  /**
   * navigate to Cart component if order is valid
   * */
  isOrderValid(isValid: boolean) {
    if (isValid) {
      this.ngWizardService.next();
    }
  }

  /**
   * navigate to Payment component if cart is valid
   * */
  isCartValid(isValid: boolean) {
    if (isValid) {
      this.ngWizardService.next();
      this.proceedToPayment = true
    }
  }

}
