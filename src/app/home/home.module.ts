import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import {NgWizardModule} from "ng-wizard";
import {OrderModule} from "../order/order.module";
import {CartModule} from "../cart/cart.module";
import {PaymentModule} from "../payment/payment.module";
import {ConfirmationModule} from "../confirmation/confirmation.module";


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgWizardModule,
    OrderModule,
    CartModule,
    PaymentModule,
    ConfirmationModule
  ]
})
export class HomeModule { }
