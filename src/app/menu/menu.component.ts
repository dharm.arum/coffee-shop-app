import { Component, OnInit } from '@angular/core';
import {ICoffeeMenu, IItem} from "../interfaces/ICoffeeMenu";
import {CoffeeAppService} from "../service/coffeeapp.service";
import {IDiscount} from "../interfaces/IDiscount";
import {Router} from "@angular/router";

/**
 * displays menu - food, beverages and combos to user, price and combo discounts
 * */
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {
  public coffeeMenu: ICoffeeMenu = {} as ICoffeeMenu;
  public discountData: IDiscount[];
  constructor(public coffeeAppService: CoffeeAppService, private router: Router) {
    this.coffeeAppService.getCoffeeMenu().subscribe((res: ICoffeeMenu) => {
      this.coffeeMenu = res;
    });
    coffeeAppService.getDiscountData().subscribe((res) => {
      this.discountData = res;
    });
  }

  ngOnInit(): void {
  }

  /**
   * Filters item ids from the coffeeMenu data and returns back IItem[]
   * */
  getItems (pItemIds: string[]): IItem[] {
    let items: IItem[] = [];
    pItemIds.map(pItemId => {
      this.coffeeMenu.menu.forEach( (menu) => {
        const item: IItem = menu.items.filter( item => item.id === pItemId)[0]
        if (item) {
          items.push(item)
        }
      })
    })
    return items;
  }

  navigateToOrder() {
    this.router.navigate(['/', 'order'])
  }
}
