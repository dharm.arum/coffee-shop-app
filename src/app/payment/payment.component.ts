import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Observable, Subscriber, Subscription} from "rxjs";
import {Router} from "@angular/router";
import {CoffeeAppService} from "../service/coffeeapp.service";

/**
 * Payment component that simulates payment
 * */
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.sass']
})
export class PaymentComponent implements OnInit, OnDestroy, OnChanges {

  public paymentStatus: string;
  private paymentObservable: Observable<string>;
  private paymentSubscription: Subscription;

  @Input()
  makePayment: boolean;

  constructor(private coffeeAppService: CoffeeAppService, private router: Router) { }

  ngOnChanges(changes: SimpleChanges): void {
    for (const prop in changes) {
      if (prop === 'makePayment') {
        if (changes[prop] && changes[prop].currentValue === true) {
          this.connectToPaymentGateway();
        }
      }
    }
  }

  ngOnInit(): void {
    this.paymentObservable = Observable.create( (observer: Subscriber<string>) => {
      observer.next("Connecting to payment gateway...");
      setTimeout(() =>{
        observer.next("Connected to payment gateway...")
      }, 1000)

      setTimeout(() =>{
        observer.next("Amount debited from your bank account...")
      }, 2000)

      setTimeout(() =>{
        observer.next("Your order will be delivered successfully")
        this.makePayment = false;
      }, 3000)
    })
  }

  connectToPaymentGateway() {
    this.paymentSubscription = this.paymentObservable.subscribe( (value: string) => {
      this.paymentStatus = value;
    })
  }

  navigateToHome() {
    this.coffeeAppService.resetCart();
    this.router.navigate(['/']);
  }
  /**
   * unsubscribe and release the memory
   * */
  ngOnDestroy() {
    this.paymentSubscription && this.paymentSubscription.unsubscribe();
  }

}
