import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentComponent } from './payment.component';


@NgModule({
    declarations: [
        PaymentComponent
    ],
    exports: [
        PaymentComponent
    ],
    imports: [
        CommonModule,
        PaymentRoutingModule
    ]
})
export class PaymentModule { }
