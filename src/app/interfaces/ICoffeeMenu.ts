export interface ICoffeeMenu {
  menu: IMenu[]
}

export interface IMenu {
  type: MENU_TYPE,
  items: IItem[]
}

export interface IItem {
  id: string,
  name: string,
  description: string,
  price: number,
  quantity: number,
  tax: number,
  icon: string
}

export enum MENU_TYPE {
  BEVERAGES = 'Beverages',
  FOOD = 'Food'
}

export interface ICart {
  items: IItem[]
}

export interface ICartItemWiseDiscount {
  itemIds: IItem[],
  discount: number,
  comboQuantity: number,
  comboPrice: number
}
