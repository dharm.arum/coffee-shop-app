export interface IDiscount {
  itemIds: string[],
  discountPercentage: number
}
