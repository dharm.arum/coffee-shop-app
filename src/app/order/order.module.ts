import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order.component';
import {NumberPickerModule} from "ng-number-picker";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [
    OrderComponent
  ],
  exports: [
    OrderComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    NumberPickerModule,
    SharedModule
  ]
})
export class OrderModule { }
