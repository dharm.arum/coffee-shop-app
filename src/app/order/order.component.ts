import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CoffeeAppService} from "../service/coffeeapp.service";
import {ICoffeeMenu, IMenu} from "../interfaces/ICoffeeMenu";

/**
 * Order component displays all the items, and allows user to select items
 * */
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.sass']
})
export class OrderComponent implements OnInit {

  @Output()
  isValid: EventEmitter<boolean> = new EventEmitter<boolean>();
  //
  public coffeeMenu: ICoffeeMenu = {} as ICoffeeMenu;
  public filteredCoffeeMenu: ICoffeeMenu=  {} as ICoffeeMenu;
  //
  constructor(public coffeeAppService: CoffeeAppService) {
    this.coffeeAppService.getCoffeeMenu().subscribe((res: ICoffeeMenu) => {
      this.coffeeMenu = res;
      this.filteredCoffeeMenu = res;
    });
  }

  /**
   * Search items as user enters few characters
   * */
  onSearchKeyPress($event: KeyboardEvent) {
    const searchValue = ($event.target as HTMLInputElement).value;
    const menu: IMenu[] = this.coffeeMenu.menu.map((menu: IMenu) => {
      return {
        ...menu,
        items: menu.items.filter(item => item.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1 )
      }
    })
    this.filteredCoffeeMenu = {
      "menu": menu
    };
  }

  ngOnInit(): void {
  }

  /**
   * navigate to cart component
   * */
  navigateToCart() {
    this.isValid.emit(true);
  }
}
