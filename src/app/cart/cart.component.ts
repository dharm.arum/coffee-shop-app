import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CoffeeAppService} from "../service/coffeeapp.service";
import {IItem} from "../interfaces/ICoffeeMenu";

/**
 * CartComponent - displays items in cart and displays billing details to user
 * */
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {

  @Output()
  isValid: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public coffeeAppService: CoffeeAppService) { }

  ngOnInit(): void {
  }

  navigateToPayment() {
    this.isValid.emit(true);
  }

}
